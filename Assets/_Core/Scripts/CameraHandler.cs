﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraHandler : MonoBehaviour
{
    public float strength = 0.5f;

    public Shader shader;
    public Material material;

    private void OnDisable()
    {
        //Make sure this doesn't run in the editor
        if (material)
        {
            DestroyImmediate(material);
        }
    }

    private void Awake()
    {
        material.SetFloat("_Strength", strength);
    }

    private void OnEnable()
    {
        shader = Shader.Find("CustomShader/Bloom");
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        material.SetFloat("_Strength", strength);
        Graphics.Blit(source, destination, material);
    }
}
