﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkManager : MonoBehaviour
{
    public GameObject GoblinPrefab;

    public int HorChunkAmount;
    public int VerChunkAmount;
    public float ChunkSize;
    public float GoblinDistance;
    public float RenderDistance;

    private float actualChunkWidth;
    private float actualChunkLength;

    private List<GameObject> chunks;

    // Start is called before the first frame update
    void Start()
    {
        chunks = new List<GameObject>();
        SpawnChunks();
    }

    // Update is called once per frame
    void Update()
    {
        int currentChunk = GetChunkFromWorld(transform.position);
        foreach (GameObject chunk in chunks)
        {
            //check currently standing in chunk
            if (chunk.name == "Chunk-" + currentChunk)
            {
                if (chunk.activeSelf == false) chunk.SetActive(true);
                continue;
            }

            Vector3 chunkPosition = chunk.GetComponent<Transform>().position;

            //check chunks that are too far away
            if (Vector3.Distance(transform.position, chunkPosition) > RenderDistance)
            {
                if (chunk.activeSelf == true) chunk.SetActive(false);
            }
            else
            {
                if (chunk.activeSelf == false) chunk.SetActive(true);
            }
        }
    }

    int GetChunkFromWorld(Vector3 position)
    {
        int iWidth = Mathf.FloorToInt(position.x / ChunkSize);
        int jLenght = Mathf.FloorToInt(position.z / ChunkSize);

        int listPosition = iWidth + (jLenght * VerChunkAmount);
        return listPosition;
    }

    private void SpawnChunks()
    {
        Vector3 goblinSize = GoblinPrefab.GetComponentInChildren<Renderer>().bounds.size;

        actualChunkWidth = (goblinSize.x + GoblinDistance) * ChunkSize;
        actualChunkLength = (goblinSize.z + GoblinDistance) * ChunkSize;

        for (int iHorChunk = 0; iHorChunk < HorChunkAmount; iHorChunk++)
        {
            for (int jVerChunk = 0; jVerChunk < VerChunkAmount; jVerChunk++)
            {
                //find/create chunk
                int chunkID = jVerChunk + (iHorChunk * VerChunkAmount);
                GameObject chunk = new GameObject("Chunk-" + chunkID.ToString());
                chunks.Add(chunk);

                //place chunk
                float chunkMiddle = (actualChunkWidth / 2) - goblinSize.x / 2;
                chunk.transform.localPosition = new Vector3((jVerChunk * actualChunkWidth) + chunkMiddle,
                                                            goblinSize.y,
                                                            (iHorChunk * actualChunkLength) + chunkMiddle);

                //place goblins in chunk
                for (int iGoblin = 0; iGoblin < ChunkSize * ChunkSize; iGoblin++)
                {
                    float goblinPosX = ((iGoblin % ChunkSize) * (goblinSize.x + GoblinDistance)) + (jVerChunk * actualChunkWidth);
                    float goblinPosY = ((Mathf.Floor(iGoblin / ChunkSize)) * (goblinSize.z + GoblinDistance)) + (iHorChunk * actualChunkLength);

                    GameObject goblin = Instantiate(GoblinPrefab, new Vector3(goblinPosX, 0.0f, goblinPosY), new Quaternion(0.0f, 0.0f, 0.0f, 0.0f));

                    goblin.GetComponent<GoblinData>().SetChunkID(chunkID);
                    goblin.SetActive(true);
                    goblin.transform.SetParent(chunk.transform);
                }

                chunk.SetActive(true);
            }
        }
    }
}
