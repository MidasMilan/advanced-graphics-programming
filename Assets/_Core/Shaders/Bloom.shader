﻿Shader "CustomShader/Bloom"
{
	Properties
	{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_Strength("Bloom Strength", Float) = 0.7
	}
		SubShader
		{
			// No culling or depth
			Cull Off ZWrite Off ZTest Always

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
				};

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = v.uv;
					return o;
				}

				sampler2D _MainTex;
				float _Strength;

				fixed4 frag(v2f i) : SV_Target
				{
					//Get the color from the texture
					fixed4 mainColor = tex2D(_MainTex, i.uv);
					fixed4 bloomColor = mainColor;

					//Transform the color by a power of the bloom color
					mainColor.rgb = pow(bloomColor.rgb, _Strength);

					//Get a fraction of the color value because the bloom effect is very strong
					mainColor.rgb *= bloomColor;

					//add the bloom color
					mainColor.rgb += bloomColor;
					return mainColor;
				}
				ENDCG
			}
		}
}
